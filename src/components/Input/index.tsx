import React, { InputHTMLAttributes } from "react";

import "./styles.css";

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string;
  name: string;
  icon: string;
}

const Input: React.FC<InputProps> = ({ label, name, icon, ...rest }) => {    
  return (
    <div className="input-block">     
      <input type="text" id={icon} {...rest} />
      <span className="focus-input100"></span>
      <span className="symbol-input100">
          <img src={require(`../../assets/images/${icon}.png`)} alt=''/>
      </span>
    </div>
  );
};

export default Input;