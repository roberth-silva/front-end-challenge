import React, { useState, useEffect, FormEvent } from 'react';

import Input from '../../components/Input';

import { FaHeadphones, FaTrash } from 'react-icons/fa';

import ListIcon from '../../assets/images/listusers.png';

import api from '../../services/api';

import { useHistory } from 'react-router-dom';

import './styles.css';


interface User{
    _id: number;
    name: number;
    cpf: string;
    email: string;
    __v: number;
}

const Users = () => {

    const [search, setSearch] = useState('');
    const [users, setUsers] = useState<User[]>([]);

    const history = useHistory();

    useEffect(() => {
        async function loadUsers(){            
            const response = await api.get('conversations');            
            setUsers(response.data);
        }
        loadUsers();
    }, []);

    function handleUser(){

    }

    async function handleDelete(id: string){        
        api.delete(`/conversation/${id}`);
        const response = await api.get('conversations');//
        setUsers(response.data);
    }
    
    return (
        <div id="page-main">
            <div className="box-users">
                <div className="box-title">
                    <img src={ListIcon} alt="User List"/>
                    <label className="text-title">Usuários</label>
                </div>

                <div className="box-body">
                   

                    <Input
                        label = "Buscar"
                        name = "Buscar"
                        icon = "phoneicon"
                        value = {search}
                        onChange={(e) => setSearch(e.target.value)}
                        placeholder = "Buscar" 
                        className = "search" 
                    />
                    
                    
                    <table>
                        
                        <tbody>
                            {users.map((user: User) => (
                                <tr key={user._id}>
                                    <td>{user.name}</td>
                                    <td>{user.cpf}</td>
                                    <td>{user.email}</td>
                                    <td>
                                        <a onClick= {() => handleUser()} >
                                            <FaHeadphones size={22} color={"#04bf58"} />
                                        </a>
                                    </td>
                                    <td>
                                        <a onClick= {() => handleDelete(String(user._id))} >
                                            <FaTrash size={22} color={"#960023"} />
                                        </a>
                                    </td>
                                </tr>                                
                            ))}
                        </tbody>
                    </table>                    
                    
                </div>

                <footer>
                    <button onClick = {() => history.push('/')}>Voltar</button>
                </footer>           

            </div>
        </div>       
    );
}

export default Users;