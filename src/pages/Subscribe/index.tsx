import React, { useState, FormEvent } from 'react';

import Input from '../../components/Input';
import Select from '../../components/Select';

import api from '../../services/api';

import './styles.css';
import { useHistory } from 'react-router-dom';

const Subscribe = () => {

    const [name, setName] = useState('');
    const [cpf, setCpf] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [uf, setUf] = useState('');
    const [subject, setSubject] = useState('');

    const history = useHistory();

    async function handleCreateConversation(e: FormEvent){
        e.preventDefault();

        const result = await api.post('conversation', {
            name,
            cpf,
            email,
            phone,
            uf,
            subject
        });        

        history.push('/users'); 
    }
    
    return (
        <div id="page-main">
            <div className="box-subscribe">
                <div className="box-title">
                    <p className="text-title">CADASTRE SUAS INFORMAÇÕES PARA INICIAR O ATENDIMENTO</p>
                </div>

                <div className="box-body">
                    <form onSubmit={handleCreateConversation}>
                                        
                        <Input 
                            name="Nome" 
                            label="nome"
                            value = {name}
                            onChange={(e) => setName(e.target.value)}
                            icon="usericon"
                            placeholder="NOME"
                        />

                        <Input 
                            name="CPF" 
                            label="nome"
                            value = {cpf}
                            onChange={(e) => setCpf(e.target.value)}
                            icon="userbordericon"
                            placeholder="CPF"
                        />

                        <Input 
                            name="Email" 
                            label="email"
                            value = {email}
                            onChange={(e) => setEmail(e.target.value)}
                            icon="mailicon"
                            placeholder="EMAIL"
                        />

                        <Input 
                            name="Telefone" 
                            label="telefone"
                            value = {phone}
                            onChange={(e) => setPhone(e.target.value)}
                            icon="phoneicon"
                            placeholder="TELEFONE"
                        />

                        <Input 
                            name="Estado" 
                            label="estado"
                            value = {uf}
                            onChange={(e) => setUf(e.target.value)}
                            icon="localicon"
                            placeholder="ESTADO"
                        />

                        <Select 
                            name="type"
                            label="Assunto"
                            value={subject}
                            onChange={(e) => setSubject(e.target.value)}
                            options={
                                [   
                                    { value: 'ELOGIO', label: 'ELOGIO' },
                                    { value: 'RECLAMAÇÃO', label: 'RECLAMAÇÃO' },
                                    { value: 'SUGESTÃO', label: 'SUGESTÃO' },
                                    { value: 'STATUS DO PEDIDO', label: 'STATUS DO PEDIDO' },
                                    { value: 'CANCELAMENTO DO PEDIDO', label: 'CANCELAMENTO DO PEDIDO' },
                                    { value: 'ESQUECI A SENHA', label: 'ESQUECI A SENHA' }
                                ]
                            }
                        />
                
                        <footer>
                            <button type="submit">Salvar</button><br/>
                            <button type="button" onClick = {() => history.push('/')}>Voltar</button>                            
                        </footer>
                        
                    </form>
                </div>
            </div>
        </div>       
    );
}

export default Subscribe;